## DOCS
https://medium.com/the-godev-corner/how-to-create-a-go-private-module-with-docker-b705e4d195c4

ssh-keygen -t ed25519 -C "aleksander.mazurov@gmail.com"

eval "$(ssh-agent -s)"
ssh-add ~/.ssh/id_github

git config --global init.defaultBranch main
git init

ssh -vT git@github.com
git remote 
git remote remove origin
git remote add origin git@github.com:remotejob/go-filter.git


go env -w GOPRIVATE=github.com/remotejob/*,gitlab.com/remotejob/*

go get github.com/remotejob/go-filter

git config --global url."git@github.com:".insteadOf "https://github.com/"
git config --global url."git@gitlab.com:".insteadOf "https://gitlab.com/"
docker build --build-arg SSH_PRIVATE_KEY="$(cat $SSH_PRIVATE_KEY)"

docker build --build-arg SSH_PRIVATE_KEY='$(cat ~/.ssh/id_github)' .

docker build --no-cache --build-arg SSH_PRIVATE_KEY="$(cat ~/.ssh/id_github)" .

docker build --no-cache --build-arg SSH_PRIVATE_KEY="$(echo $SSH_PRIVATE_KEY)" .


docker buildx rm --all-inactive --force

docker buildx use $(docker buildx create --platform linux/amd64,linux/arm64)

Try jenkins
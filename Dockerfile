# syntax=docker/dockerfile:1
## Build
FROM --platform=$BUILDPLATFORM golang:alpine AS build
WORKDIR /src

LABEL maintainer="Remotejob"

RUN apk add -U tzdata
RUN apk --update add ca-certificates

RUN apk add --no-cache openssh
RUN apk add --no-cache git

ARG SSH_PRIVATE_KEY=$SSH_PRIVATE_KEY

RUN mkdir -p -m 0700 ~/.ssh && ssh-keyscan github.com >> ~/.ssh/known_hosts

RUN echo "${SSH_PRIVATE_KEY}" >  ~/.ssh/id_ed25519
RUN chmod 600 ~/.ssh/id_ed25519

RUN git config --global url."git@github.com:".insteadOf "https://github.com/"

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

ARG TARGETOS TARGETARCH
RUN --mount=target=. \
    --mount=type=cache,target=/root/.cache/go-build \
    --mount=type=cache,target=/go/pkg \
    GOOS=$TARGETOS GOARCH=$TARGETARCH CGO_ENABLED=0 go build -o /main ./...

## Deploy
FROM scratch 

COPY --from=build /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=build /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=build /etc/passwd /etc/passwd
COPY --from=build /etc/group /etc/group
COPY --from=build /main .

ENTRYPOINT ["./main"]
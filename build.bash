#!/bin/bash 
# export $(egrep -v ‘^#’ .env | xargs) && docker buildx build -t "remotejob/go-private-example" --no-cache --build-arg SSH_PRIVATE_KEY="${SSH_PRIVATE_KEY}" --platform linux/amd64,linux/arm64 --push .
source .env && docker buildx build  -t "remotejob/go-private-example" --no-cache --build-arg SSH_PRIVATE_KEY="${SSH_PRIVATE_KEY}" --platform linux/amd64,linux/arm64 --push .